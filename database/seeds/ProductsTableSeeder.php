<?php

use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $apple = Supplier::where('name', 'Apple')->first();

        factory(Product::class)->create([
            'name' => 'Airport Extreme',
            'description' => 'Tim Cook\'s very best WiFi access point.',
            'supplier_id' => $apple->getKey(),
        ]);

        factory(Product::class)->create([
            'name' => 'Airport Express',
            'description' => 'Tim Cook\'s cheapest WiFi access point.',
            'supplier_id' => $apple->getKey(),
        ]);

        factory(Product::class, 30)->create();
    }
}
