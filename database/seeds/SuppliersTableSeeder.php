<?php

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    public function run()
    {
        factory(Supplier::class)->create([
            'name' => 'Apple',
        ]);

        factory(Supplier::class, 5)->create();
    }
}
