<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Product;
use App\Models\Supplier;
use Ramsey\Uuid\Uuid;

$factory->define(Product::class, function (Faker\Generator $faker) {
    static $suppliers;

    if (!$suppliers) {
        $suppliers = Supplier::where('name', '<>', 'Apple')->get()->pluck('id');
    }

    return [
        'id' => (string) Uuid::uuid4(),
        'name' => $faker->words(2, true),
        'description' => $faker->sentence(),
        'price' => $faker->randomFloat(2, 0, 1000),
        'stock' => $faker->numberBetween(0, 50),
        'supplier_id' => $suppliers->random(),
    ];
});

$factory->define(Supplier::class, function (Faker\Generator $faker) {
    return [
        'id' => (string) Uuid::uuid4(),
        'name' => $faker->firstNameFemale,
    ];
});
