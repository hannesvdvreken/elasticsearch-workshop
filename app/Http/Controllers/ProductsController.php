<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use App\Products\Commands\RemoveProduct;
use App\Products\Commands\StoreProduct;
use App\Products\Queries\ElasticsearchQuery;
use Illuminate\Routing\Controller as BaseController;

class ProductsController extends BaseController
{
    /**
     * @var ElasticsearchQuery
     */
    private $query;

    /**
     * @param ElasticsearchQuery $query
     */
    public function __construct(ElasticsearchQuery $query)
    {
        $this->query = $query;
    }

    public function index()
    {
        $search = request('q');
        $supplierId = request('supplier');

        $products = $this->query->search($search, $supplierId);

        return view('products.index', ['products' => $products, 'search' => $search]);
    }

    public function create()
    {
        return view('products.create', ['suppliers' => Supplier::all()]);
    }

    public function store()
    {
        dispatch(new StoreProduct(
            (string) request('name'),
            (string) request('description'),
            (string) request('supplier'),
            (float) request('price'),
            (int) request('stock')
        ));

        return redirect('/products')->with('message', sprintf('Product "%s" created', request('name')));
    }

    public function destroy()
    {
        dispatch(new RemoveProduct(request()->route('product')));

        return redirect()->back()->with('message', 'Product has been deleted successfully');
    }
}
