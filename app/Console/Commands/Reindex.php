<?php

namespace App\Console\Commands;

use App\Models\Product;
use DateTime;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Illuminate\Console\Command;

class Reindex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex all searchable documents';

    /**
     * @return mixed
     */
    public function handle()
    {
        // Create elasticsearch client.
        $elasticsearch = ClientBuilder::create()->build();

        try {
            $index = $elasticsearch->indices()->get(['index' => 'products']);
            $removeOldIndex = true;
            $oldIndexName = array_first(array_keys($index));
        } catch (Missing404Exception $exception) {
            $removeOldIndex = false;
            $oldIndexName = false;
        }

        $settings = [
            'analysis' => [
                'analyzer' => [
                    'custom_text_analyzer' => [
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                        'filter' => [
                            'lowercase',
                            'asciifolding',
                            'custom_edge_ngram',
                        ],
                    ],
                    'custom_uuid_analyzer' => [
                        'type' => 'custom',
                        'tokenizer' => 'whitespace',
                        'filter' => [
                            'custom_uuid_edge_ngram',
                        ],
                        'char_filter' => [
                            'uuid_dash_remove',
                        ],
                    ],
                ],
                'filter' => [
                    'custom_edge_ngram' => [
                        'type' => 'edge_ngram',
                        'min_gram' => '3',
                        'max_gram' => '10',
                    ],
                    'custom_uuid_edge_ngram' => [
                        'type' => 'edge_ngram',
                        'min_gram' => '3',
                        'max_gram' => '32',
                    ],
                ],
                'char_filter' => [
                    'uuid_dash_remove' => [
                        'type' => 'pattern_replace',
                        'pattern' => '-',
                        'replacement' => '',
                    ],
                ],
            ],
        ];

        $mappings = [
            'product' => [
                'properties' => [
                    'id' => [
                        'type' => 'text',
                        'analyzer' => 'custom_uuid_analyzer',
                    ],
                    'name' => [
                        'type' => 'text',
                        'analyzer' => 'custom_text_analyzer',
                    ],
                    'description' => [
                        'type' => 'text',
                        'analyzer' => 'custom_text_analyzer',
                    ],
                    'supplier' => [
                        'type' => 'text',
                        'analyzer' => 'custom_text_analyzer',
                    ],
                    'supplier_id' => [
                        'type' => 'keyword',
                        'index' => 'true',
                    ],
                ],
            ],
        ];

        $newIndexName = 'products-'.(new DateTime())->getTimestamp();
        $elasticsearch->indices()->create(['index' => $newIndexName, 'body' => compact('mappings', 'settings')]);

        // Loop products and index them.
        $this->output->progressStart();
        $size = 100;

        Product::chunk($size, function ($products) use ($elasticsearch, $newIndexName, $size) {
            $operations = [];
            foreach ($products as $product) {
                $operations[] = [
                    'index' => [
                        '_index' => $newIndexName,
                        '_type' => 'product',
                        '_id' => $product->id,
                    ],
                ];
                $operations[] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'description' => $product->description,
                    'supplier' => $product->supplier->name,
                    'supplier_id' => $product->supplier->id,
                ];
            }

            $elasticsearch->bulk(['body' => $operations]);

            $this->output->progressAdvance($size);
        });

        $this->output->progressFinish();

        // Drop the old index.
        if ($removeOldIndex) {
            $elasticsearch->indices()->updateAliases([
                'body' => [
                    'actions' => [
                        ['remove' => ['index' => $oldIndexName, 'alias' => 'products']],
                        ['add' => ['index' => $newIndexName, 'alias' => 'products']],
                    ],
                ],
            ]);

            $elasticsearch->indices()->delete(['index' => $oldIndexName]);
        } else {
            $elasticsearch->indices()->updateAliases([
                'body' => [
                    'actions' => [
                        ['add' => ['index' => $newIndexName, 'alias' => 'products']],
                    ],
                ],
            ]);
        }
    }
}
