<?php

namespace App\Providers;

use App\Products\Events\ProductWasCreated;
use App\Products\Events\ProductWasRemoved;
use App\Products\Listeners\DeleteFromIndex;
use App\Products\Listeners\IndexForSearch;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ProductWasCreated::class => [
            IndexForSearch::class,
        ],
        ProductWasRemoved::class => [
            DeleteFromIndex::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
