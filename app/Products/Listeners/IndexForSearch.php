<?php

namespace App\Products\Listeners;

use App\Models\Product;
use App\Products\Events\ProductWasCreated;
use Elasticsearch\ClientBuilder;

class IndexForSearch
{
    /**
     * @param ProductWasCreated $event
     */
    public function handle(ProductWasCreated $event)
    {
        // Create ES SDK
        $elasticsearch = ClientBuilder::create()->build();

        // Fetch product
        $product = Product::find($event->getProductId());

        // Index product
        $elasticsearch->create([
            'index' => 'products',
            'type' => 'product',
            'id' => $product->id,
            'body' => [
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'supplier' => $product->supplier->name,
            ],
        ]);
    }
}
