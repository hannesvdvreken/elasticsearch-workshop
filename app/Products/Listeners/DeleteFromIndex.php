<?php

namespace App\Products\Listeners;

use App\Models\Product;
use App\Products\Events\ProductWasRemoved;
use Elasticsearch\ClientBuilder;

class DeleteFromIndex
{
    /**
     * @param ProductWasRemoved $event
     */
    public function handle(ProductWasRemoved $event)
    {
        // Create ES SDK
        $elasticsearch = ClientBuilder::create()->build();

        // Remove product from index
        $elasticsearch->delete([
            'index' => 'products',
            'type' => 'product',
            'id' => $event->getProductId(),
        ]);
    }
}
