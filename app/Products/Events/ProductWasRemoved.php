<?php

namespace App\Products\Events;

class ProductWasRemoved
{
    /**
     * @var string
     */
    private $productId;

    /**
     * @param string $productId
     */
    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
