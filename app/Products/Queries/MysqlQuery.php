<?php

namespace App\Products\Queries;

use App\Models\Product;

class MysqlQuery implements Query
{
    /**
     * @param string $search
     * @param string $supplierId
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search($search = null, $supplierId = null)
    {
        if (!$search) {
            return Product::paginate();
        }

        $products = Product::where('name', 'like', "%$search%")
            ->orWhere('description', 'like', "%$search%")
            ->orWhere('id', 'like', "$search%")
            ->orWhereHas('supplier', function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            })
            ->paginate();

        return $products->appends('q', $search);
    }
}
