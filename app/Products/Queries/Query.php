<?php

namespace App\Products\Queries;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface Query
{
    /**
     * @param string $search
     *
     * @return LengthAwarePaginator
     */
    public function search($search = null, $supplierId = null);
}
