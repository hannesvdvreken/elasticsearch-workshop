<?php

namespace App\Products\Queries;

use App\Models\Product;
use Elasticsearch\ClientBuilder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class ElasticsearchQuery implements Query
{
    /**
     * @param string $search
     * @param string $supplierId
     *
     * @return LengthAwarePaginator
     */
    public function search($search = null, $supplierId = null)
    {
        if (!$search && !$supplierId) {
            return Product::paginate();
        }

        // Mimicking Eloquent default pagination behaviour.
        $perPage = 15;
        $page = Paginator::resolveCurrentPage('page');
        $path = Paginator::resolveCurrentPath();

        // Create Elasticsearch client.
        $elasticsearch = ClientBuilder::create()->build();

        $searchQuery = [
            'multi_match' => [
                'query' => $search,
                'fields' => ['id', 'name^2', 'description', 'supplier'],
                'fuzziness' => 'AUTO',
            ],
        ];

        $supplierQuery = [
            'term' => [
                'supplier_id' => $supplierId,
            ],
        ];

        if ($supplierId && $search) {
            $query = [
                'bool' => [
                    'must' => [
                        $supplierQuery,
                        $searchQuery,
                    ],
                ],
            ];
        } elseif ($search) {
            $query = $searchQuery;
        } elseif ($supplierId) {
            $query = $supplierQuery;
        }

        // Perform paginated query.
        $response = $elasticsearch->search([
            'index' => 'products',
            'type' => 'product',
            'size' => $perPage,
            'from' => $perPage * ($page - 1),
            'body' => [
                'query' => $query,
            ],
        ]);

        // Get total of products returned by this query.
        $total = $response['hits']['total'];

        // Get all Eloquent models.
        $ids = collect($response['hits']['hits'])->pluck('_id')->all();

        $products = Product::find($ids);

        $products = $products->sort(function ($a, $b) use ($ids) {
            $positionA = array_search($a->id, $ids, true);
            $positionB = array_search($b->id, $ids, true);

            return $positionA <=> $positionB;
        });

        // Return paginator.
        return (new LengthAwarePaginator(
            $products, $total, $perPage, $page, ['path' => $path]
        ))->appends('q', $search)->appends('supplier', $supplierId);
    }
}