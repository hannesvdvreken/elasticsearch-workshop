<?php

namespace App\Products\Commands;

use App\Models\Product;
use App\Products\Events\ProductWasCreated;
use Ramsey\Uuid\Uuid;

class StoreProduct
{
    private $name;
    private $description;
    private $price;
    private $stock;
    private $supplierId;

    /**
     * @param string $name
     * @param string $description
     * @param string $supplierId
     * @param number $price
     * @param int $stock
     */
    public function __construct(string $name, string $description, string $supplierId, $price, int $stock)
    {
        $this->name = $name;
        $this->description = $description;
        $this->supplierId = $supplierId;
        $this->price = $price;
        $this->stock = $stock;
    }

    /**
     *
     */
    public function handle()
    {
        Product::create([
            'id' => $id = Uuid::uuid4(),
            'name' => $this->name,
            'description' => $this->description,
            'supplier_id' => $this->supplierId,
            'price' => $this->price,
            'stock' => $this->stock,
        ]);

        event(new ProductWasCreated($id));
    }
}
