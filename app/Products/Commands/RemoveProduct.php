<?php

namespace App\Products\Commands;

use App\Models\Product;
use App\Products\Events\ProductWasRemoved;

class RemoveProduct
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function handle()
    {
        Product::destroy($this->id);

        event(new ProductWasRemoved($this->id));
    }
}
