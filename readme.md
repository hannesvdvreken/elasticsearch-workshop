# Elasticsearch workshop

## Getting started

Checkout this repo, install all dependencies, setup your environment file, and compile all assets.

```
git clone git@github.com:hannesvdvreken/elasticsearch-workshop
```

```
# install dependencies
composer install
yarn install # or npm install

# setup your .env file and generate a key for security.
cp .env.example .env
php artisan key:generate

# compile assets
npm run dev
```

Now we want to get our DB running: migrated and seeded. Indexing the Elasticsearch index can also be done:

```
php artisan migrate --seed
php artisan search:reindex
```

### Elasticsearch

If needed, you can quickly install and run Elasticsearch on a Debian based system with the following script:

```
./install_elasticsearch.php
```

## Extra assignments

### Filter on supplier ID in products index page

Make the following url work:

```
/products?q={term}&supplier={id}
```

This shows products for a specific supplier only.
The supplier ID must be the unique ID of the supplier
and there must be an exact match.

The search box on the page must still work to further search for products
from that specific supplier. Make sure pagination still works too.

For the mapping of the supplier ID in the products index:

https://www.elastic.co/guide/en/elasticsearch/reference/current/keyword.html

### Refactor ReindexCommand for zero downtime re-indexing on deployment

The trick is to use a generated index name, and to create a new alias
for that index for use throughout the application. When you redeploy,
you need to create a new index with a new name, fill it with data
and move the alias from the old index to the new one. Make sure this works
on the first run of the reindex command (`php artisan search:reindex`)
and on all future runs too (idempotent).

https://www.elastic.co/guide/en/elasticsearch/guide/current/index-aliases.html

### Batch index products

Change the amount of products in your database by increasing the amount in the
`ProductsTableSeeder` to 10000 products. The idea is to not do 10k round trips
to Elasticsearch but 100 calls with 100 inserts, for example. You can experiment with
batch size to find an optimal size between less round trips and smaller
HTTP requests bodies to Elasticsearch (and less memory usage in PHP).

Use `Product::chunk($batchSize, function ($products) {})` to fetch batches of
products from the `products` table. Batch insert those in Elasticsearch in one single call.

https://www.elastic.co/guide/en/elasticsearch/client/php-api/current/_indexing_documents.html#_bulk_indexing

## License

The code in here is proprietary and thus not free to distribute.
