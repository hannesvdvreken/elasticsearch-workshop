<tr>
    <th scope="row">{{ substr($product['id'], 0, 8) }}...</th>
    <td>{{ $product['name'] }}</td>
    <td>{{ $product['description'] }}</td>
    <td>{{ $product['supplier']['name'] }}</td>
    <td>
        <form action="/products/{{ $product['id'] }}" method="POST" class="pull-right">
            <input type="hidden" name="_method" value="DELETE" />
            {{ csrf_field() }}
            <input type="submit" class="btn btn-danger" value="delete" />
        </form>
    </td>
</tr>
