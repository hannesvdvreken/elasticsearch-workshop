<div class="col-lg-12">
    <div class="panel panel-warning">
        <div class="panel-heading">No products found</div>
        <div class="panel-body">
            <p>Want to create one?</p>
            <a class="btn btn-info" href="/products/create">create product</a>
        </div>
    </div>
</div>
