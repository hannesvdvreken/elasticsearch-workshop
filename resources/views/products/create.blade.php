@extends('layouts.app')

@section('content')
    <div class="row">
        <h1 class="page-header col-lg-12">Create a new product</h1>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form class="form-horizontal" action="/products" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Name</label>
                    <input class="form-control" name="name" placeholder="name">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" name="description" placeholder="description" style="resize: vertical;"></textarea>
                </div>
                <div class="form-group">
                    <label for="supplier">Supplier</label>
                    <select class="form-control" name="supplier">
                        @foreach ($suppliers as $supplier)
                            <option value="{{ $supplier['id'] }}">{{ $supplier['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Unit price (€)</label>
                    <input class="form-control" type="number" name="price" placeholder="10" min="0"/>
                </div>
                <div class="form-group">
                    <label for="stock">Stock</label>
                    <input class="form-control" type="number" name="stock" placeholder="0" min="0" value="0"/>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection
