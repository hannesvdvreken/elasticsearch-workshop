@extends('layouts.app')

@section('content')
    <div class="row">
        <h1 class="page-header col-lg-12">Our products</h1>
    </div>

    @if (Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

    <div class="row">
        <form class="form-horizontal">
            <div class="col-lg-12">
                <input class="form-control" name="q" placeholder="search" value="{{ $search }}">
            </div>
        </form>
    </div>

    <div class="row">
        @if ($products->isEmpty())
            @include('partials.products.empty')
        @else
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading clearfix">
                        <h4 class="pull-left">Products</h4>
                        <div class="pull-right">
                            <a class="btn btn-default" href="/products/create">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>

                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Supplier</th>
                                <th></th>
                            </tr>
                        </thead>
                        @foreach($products as $product)
                            @include('partials.products.row')
                        @endforeach
                    </table>
                </div>
                {{ $products->links() }}
            </div>
        @endif
    </div>
@endsection
