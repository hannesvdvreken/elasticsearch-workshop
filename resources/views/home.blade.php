@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="jumbotron">
            <h1>Hello there!</h1>
            <p>Welcome to this workshop</p>
            <p>
                <a href="/products" class="btn btn-primary btn-lg" role="button">Check out our products</a>
            </p>
        </div>
    </div>
@endsection
